new Vue({
    el: '#app',
    data: {
        inputNumber: '',
        primeNumbers: [],
    },
    watch: {
        inputNumber: function () {
            this.primeNumbers = [];
        },
    },
    methods: {
        findPrimes() {
            const n = parseInt(this.inputNumber);
            if (isNaN(n) || n <= 1) {
                alert('Please enter a valid number greater than 1.');
                this.inputNumber = '';
                return;
            }
 
            this.primeNumbers = this.getPrimeNumbers(n);
        },
        getPrimeNumbers(n) {
            const primes = [];
            for (let i = 2; i <= n; i++) {
                if (this.isPrime(i)) {
                    primes.push(i);
                }
            }
            return primes;
        },
        isPrime(num) {
            for (let i = 2; i < num; i++) {
                if (num % i === 0) {
                    return false;
                }
            }
            return num !== 1;
        },
    },
});
 